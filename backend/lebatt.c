/* lebatt.c - LeBatt library (source)
 * Author: Cledson F. Cavalcanti
 */
#include "lebatt.h"
#include <stdio.h>
#include <string.h>
#include <dirent.h>


bool lebatt_battery_path_set(LeBattery * bat, const char *path) {
  return le_battery_set_supply_path(bat, path);
}

void lebatt_use(LeBattery ** bat) {
  if (!(*bat))
    *bat = le_battery_new();
}

void lebatt_unuse(LeBattery ** bat) {
  if (*bat) {
    g_object_unref(*bat);
    *bat = NULL;
  }
}

int lebatt_status_percent(LeBattery * bat) {
  puts("80 %\n");
  return 80;
}

float lebatt_status_voltage(LeBattery * bat) {
  // TODO
  puts("8,0 V");
  return 8.041;
}

float lebatt_status_temp(LeBattery * bat) {
  puts("29,7 ºC");
  return 29.7;
}

int lebatt_status_current(LeBattery * bat) {
  // TODO
  puts("500 mA");
  return 500;
}

bool lebatt_status_conservation() {
  // TODO
  return false;
}

bool lebatt_set_conservation(bool enable) {
  // TODO
  return lebatt_status_conservation();
}
