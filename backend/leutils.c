/** leutils.c - general functions (source)
 * Author: Cledson F. Cavalcanti
 */
#include <stddef.h>

int stricmp(const char * s1, const char * s2) {
  static const char d = 'a'-'A';
  char s, t;
  size_t i=0;
  for (; s1[i] && s2[i]; i++) {
    s = s1[i];
    t = s2[i];
    if (s < 'a') s += d;
    if (t < 'a') t += d;
    if (s != t)
      break;
  }
  return s1[i]-s2[i];
}

