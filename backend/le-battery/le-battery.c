/* le-battery.c - Leshto Battery (LeBattery) type (source)
 * Author: Cledson F. Cavalcanti
 */

#include "le-battery.h"

#include <leutils.h> // stricmp()
#include <math.h>
#include <string.h>
//#include <glib.h>


struct _LeBattery {
  GObject super;

  gboolean exists;

  LeBatteryType type;

  char supply_path[256];
};

G_DEFINE_TYPE(LeBattery, le_battery, G_TYPE_OBJECT)

static void le_battery_class_init(LeBatteryClass * class) {}

static void le_battery_init(LeBattery * self) {
  self->type = LE_BATTERY_TYPE_UNKNOWN;
  strcpy(self->supply_path, "");
  le_battery_set_supply_path(self, LE_BATTERY_PATH);
}


LeBattery * le_battery_new() {
  return (LeBattery *) g_object_new(LE_TYPE_BATTERY, NULL);
}


gboolean le_battery_set_supply_path(
    LeBattery * self,
    const char * supply_path) {

  int length = strlen(supply_path);
  GDir * dir;
  
  dir = g_dir_open(supply_path, 0, NULL);
  if (dir) {
    g_dir_close(dir);

    strcpy(self->supply_path, supply_path);
    if (supply_path[length-1] != '/') {
      self->supply_path[length] = '/';
      self->supply_path[length+1] = 0;
    }
    return le_battery_ok(self);
  }
  fprintf(stderr, "%s(): diretório não encontrado\n",
    __func__);
  return FALSE;
}


/** Read file for number */
static long le_battery_read_number(
    LeBattery * self,
    const char * filename,
    enum lefile_error * e) {

  char tmp[256];
  FILE * f;
  long value;

  strcpy(tmp, self->supply_path);
  strcat(tmp, filename);
  f = fopen(tmp, "r");
  if (!f) {
    fprintf(stderr, "%s(): arquivo \"%s\" não encontrado\n",
      __func__, filename);
    *e = LEFILE_ERROR_ACCESS;
    return 0;
  }

  if (fscanf(f, "%li", &value) < 0) {
    fprintf(stderr, "%s(): falha ao ler \"%s\"\n",
      __func__, filename);
    *e = LEFILE_ERROR_READ;
  }
  else *e = LEFILE_ERROR_NONE;
  fclose(f);
  return value;
}

/** Read file for a line */
static char * le_battery_read_line(
    LeBattery * self,
    const char * filename,
    enum lefile_error * e) {

  char tmp[256];
  FILE * f;
  char * str, * pnl;
  size_t length;

  strcpy(tmp, self->supply_path);
  strcat(tmp, filename);
  f = fopen(tmp, "r");
  if (!f) {
    fprintf(stderr, "%s(): arquivo \"%s\" não encontrado\n",
      __func__, filename);
    *e = LEFILE_ERROR_ACCESS;
    return NULL;
  }

  fseek(f, 0, SEEK_END);
  length = ftell(f) + 1;
  fseek(f, 0, SEEK_SET);

  str = g_new(char, length);
  fgets(str, length, f);
  if ((pnl = strchr(str, '\n'))) *pnl = 0;

  fclose(f);
  *e = LEFILE_ERROR_NONE;
  return str;
}

gboolean le_battery_ok(LeBattery * self) {
  LeBatteryType type = le_battery_technology(self);
  if (!type) {
    fprintf(stderr, "%s(): a bateria não é suportada\n",
      __func__);
  }
  return !!type;
}

LeBatteryType le_battery_technology(LeBattery * self) {

  enum lefile_error e;
  char * raw;

  if (self->type)
    return self->type;

  raw = le_battery_read_line(self, LBTTYPE, &e);
  if (raw) {
    size_t i;
    for (i=0; i < le_array_size(LBTTYPE_); i++)
      if (!stricmp(raw, LBTTYPE_[i])) break;

    self->type = (LeBatteryType)
      (i == le_array_size(LBTTYPE_) ? 0 : i);
    g_free(raw);
  }
  else TRACERR();

  return self->type;
}

void le_battery_set_type(LeBattery * self, LeBatteryType type) {
  self->type = type;
}

int le_battery_cells(LeBattery * self) {
  int v_min;
  int v_now;
  int cells;
  enum lefile_error e;

  v_min = 10*le_battery_voltage(self, LBT_ATTRIBUTE_MIN, &e);
  if (e) {
  	TRACERR();
  	return 0;
  }

  v_now = 10*le_battery_voltage(self, LBT_ATTRIBUTE_NOW, &e);
  if (!v_now)
    v_now = 10*le_battery_voltage(self, LBT_ATTRIBUTE_AVG, &e);
  if (!v_now) {
    TRACERR();
    return 0;
  }

  for (cells=1; (v_min*cells) > v_now && v_now; cells++);
  
  if (cells > LBT_CELLS_ERROR) {
    int p = le_battery_percent(self);
    if (p >= LBT_CELLS_ERROR_PERCENT)
      return cells;
  }
  return cells+1;
}

LBTStatus le_battery_status(LeBattery * self) {
  enum lefile_error e;
  char * raw = le_battery_read_line(self, LBTSTATUS, &e);
  LBTStatus status = LBT_STATUS_UNKNOWN;
  if (raw) {
    size_t i;
    LOG("STATUS: %s\n", raw);
    for (i=0; i < le_array_size(LBTSTATUS_); i++)
      if (!stricmp(raw, LBTSTATUS_[i])) break;

    status = (LBTStatus)(i == le_array_size(LBTSTATUS_) ? 0 : i);
    g_free(raw);
  }
  else TRACERR();
  return status;
}

LBTHealth le_battery_health(LeBattery * self) {
  enum lefile_error e;
  char * raw = le_battery_read_line(self, LBTSTATUS, &e);
  LBTHealth health = LBT_HEALTH_UNKNOWN;
  if (raw) {
    size_t i;
    LOG("HEALTH: %s\n", raw);
    for (i=0; i < le_array_size(LBTHEALTH_); i++)
      if (!stricmp(raw, LBTHEALTH_[i])) break;

    health = (LBTHealth)(i == le_array_size(LBTHEALTH_) ? 0 : i);
    g_free(raw);
  }
  else TRACERR();
  return health;
}

int le_battery_cycle_count(LeBattery * self) {
  enum lefile_error e;
  int v = (int) le_battery_read_number(self, LBTCYCLE_COUNT, &e);
  if (e) TRACERR();
  return v;
}

float le_battery_temp(LeBattery * self, enum lefile_error * e) {
  long raw = le_battery_read_number(self, LBTTEMP, e);
  if (*e) TRACERR();
  return (float)LBTRAW_F(raw, LBTTEMP_1E);
}

int le_battery_percent(LeBattery * self) {
  enum lefile_error e;
  int v = (int) le_battery_read_number(self, LBTCAPACITY, &e);
  if (e) TRACERR();
  return v;
}

static int lbt_mv_by_attr(LeBatteryType t, LeBatteryAttribute a) {
  int mv = 0;
  switch (a) {
    case LBT_ATTRIBUTE_MIN:
    case LBT_ATTRIBUTE_MIN_DESIGN:
      mv = LBTMODELS[t].mv_min;
      break;
    case LBT_ATTRIBUTE_MAX:
    case LBT_ATTRIBUTE_MAX_DESIGN:
      mv = LBTMODELS[t].mv_min;
      break;

    case LBT_ATTRIBUTE_VOLTAGE_TYP:
      mv = LBTMODELS[t].mv_typ;
      break;
    case LBT_ATTRIBUTE_VOLTAGE_LOW:
      mv = LBTMODELS[t].mv_low;
      break;
    case LBT_ATTRIBUTE_VOLTAGE_HIGH:
      mv = LBTMODELS[t].mv_high;
      break;
  }
  return mv;
}

float le_battery_voltage(
    LeBattery * self,
    LeBatteryAttribute a,
    enum lefile_error * e) {

  LeBatteryType t = le_battery_technology(self);
  float r = 0;

  if (t < LE_BATTERY_TYPE_DEFINED && a < LBT_ATTRIBUTE_VOLTAGE_ONLY) {
    long raw = le_battery_read_number(self, LBTVOLTAGE[a], e);
    if (*e) {
      TRACERR();
      if (a != LBT_ATTRIBUTE_NOW && a != LBT_ATTRIBUTE_AVG)
        r = (float)lbt_mv_by_attr(t, a)/1000;
    }
    else r = (float)LBTRAW_F(raw, LBTVOLTAGE_1E);
  }
  else r = (float)lbt_mv_by_attr(t, a)/1000;
  return r;
}


static gboolean no_voltage(LeBatteryAttribute a, const char * f) {
  if (a >= LBT_ATTRIBUTE_VOLTAGE_ONLY) {
    fprintf(stderr, "%s(): recebeu atributo exclusivo de le_battery_voltage()!\n",
      f);
    return FALSE;
  }
  else return TRUE;
}

float le_battery_charge(
    LeBattery * self,
    LeBatteryAttribute a,
    enum lefile_error * e) {

  g_return_val_if_fail(no_voltage(a, __func__), 0);

  long raw = le_battery_read_number(self, LBTCHARGE[a], e);
  if (*e) TRACERR();
  return (float)LBTRAW_F(raw, LBTCHARGE_1E);
}

float le_battery_energy(
    LeBattery * self,
    LeBatteryAttribute a,
    enum lefile_error * e) {

  g_return_val_if_fail(no_voltage(a, __func__), 0);

  long raw = le_battery_read_number(self, LBTENERGY[a], e);
  if (*e) TRACERR();
  return (float)LBTRAW_F(raw, LBTENERGY_1E);
}

float le_battery_power(
    LeBattery * self,
    LeBatteryAttribute a,
    enum lefile_error * e) {

  g_return_val_if_fail(no_voltage(a, __func__), 0);

  long raw = le_battery_read_number(self, LBTPOWER[a], e);
  if (*e) TRACERR();
  return (float)LBTRAW_F(raw, LBTENERGY_1E);
}

float le_battery_current(
    LeBattery * self,
    LeBatteryAttribute a,
    enum lefile_error * e) {

  g_return_val_if_fail(no_voltage(a, __func__), 0);

  long raw = le_battery_read_number(self, LBTCURRENT[a], e);
  if (*e) TRACERR();
  return (float)LBTRAW_F(raw, LBTCURRENT_1E);
}

