/* lbt-power-supply.h - strings according to power_supply sysfs class ref
 * Author: Cledson F. Cavalcanti
 * Reference:
 * -> https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power
 * -> https://www.kernel.org/doc/html/latest/power/power_supply_class.html
 */

#ifndef _LE_BATTERY_POWER_SUPPLY_H_
#define _LE_BATTERY_POWER_SUPPLY_H_

/* ==== FILES ==== */

#define LBTCYCLE_COUNT "cycle_count"
#define LBTCAPACITY "capacity"
#define LBTTYPE "technology"
#define LBTHEALTH "health"
#define LBTSTATUS "status"
#define LBTTEMP "temp"

static const char *const LBTCURRENT[] = {
  "current_min",
  "current_max",
  "current_now",
  "current_avg",
  "current_min_design",
  "current_max_design"
};

static const char *const LBTVOLTAGE[] = {
  "voltage_min",
  "voltage_max",
  "voltage_now",
  "voltage_avg",
  "voltage_min_design",
  "voltage_max_design"
};

static const char *const LBTCHARGE[] = {
  "charge_empty",
  "charge_full",
  "charge_now",
  "charge_avg",
  "charge_empty_design",
  "charge_full_design"
};

static const char *const LBTENERGY[] = {
  "energy_empty",
  "energy_full",
  "energy_now", // or power_now
  "energy_avg",
  "energy_empty_design",
  "energy_full_design"
};

/* some programmers don't follow standards */
static const char *const LBTPOWER[] = {
  "power_empty",
  "power_full",
  "power_now",
  "power_avg",
  "power_empty_design",
  "power_full_design"
};




/* ==== METHODS ==== */
#define LBTRAW_F(X, LBTFILE_1E) ((double)(X)/pow(10, (-(LBTFILE_1E))))

#define LBTCURRENT_1E -6
#define LBTVOLTAGE_1E -6
#define LBTCHARGE_1E -6
#define LBTENERGY_1E -6
#define LBTTEMP_1E -1


/* ==== DICTIONARIES ==== */

/* We only support Li-ion and Li-polymer batteries */ 
enum le_battery_type {
  /* undefined/power_supply types */
  LE_BATTERY_TYPE_UNKNOWN = 0,
  // Li-ion-like batteries
  LE_BATTERY_TYPE_LION,
  // Li-polymer batteries
  LE_BATTERY_TYPE_LIPO,

  /* defined types (no power_supply docummented type) */
  // 3.6 V batteries
  LE_BATTERY_TYPE_LION1,
  // 3.7 V batteries
  LE_BATTERY_TYPE_LION2,

  // 3.7 V
  LE_BATTERY_TYPE_LIPO1,
  // 3.85 V
  LE_BATTERY_TYPE_LIPO2
};

#define LE_BATTERY_TYPE_DEFINED LE_BATTERY_TYPE_LION1

/* supporting Li-ion batteries only */
static const char *const LBTTYPE_[] = {
  "Unknown",
  "Li-ion",
  "Li-poly",

  "Li-ion 3.6 V (by Leshto Batt)",
  "Li-ion 3.7 V (by Leshto Batt)",
  "Li-poly 3.7 V (by Leshto Batt)",
  "Li-poly 3.85 V (by Leshto Batt)"
};


enum lbt_status {
  LBT_STATUS_UNKNOWN = 0,
  LBT_STATUS_CHARGING,
  LBT_STATUS_DISCHARGING,
  LBT_STATUS_NOT_CHARGING,
  LBT_STATUS_FULL
};

static const char * LBTSTATUS_[] = {
  "Unknown",
  "Charging",
  "Discharging",
  "Not charging",
  "Full"
};

enum lbt_health {
  LBT_HEALTH_UNKNOWN = 0,
  LBT_HEALTH_GOOD,
  LBT_HEALTH_OVERHEAT,
  LBT_HEALTH_DEAD,
  LBT_HEALTH_OVER_VOLTAGE,
  LBT_HEALTH_UNSPECIFIED_FAILURE,
  LBT_HEALTH_COLD,
  LBT_HEALTH_WATCHDOG_TIMER_EXPIRE,
  LBT_HEALTH_SAFETY_TIMER_EXPIRE,
  LBT_HEALTH_OVER_CURRENT,
  LBT_HEALTH_CALIBRATION_REQUIRED
};

static const char * LBTHEALTH_[] = {
  "Unknown",
  "Good",
  "Overheat",
  "Dead",
  "Over voltage",
  "Unspecified failure"
  "Cold",
  "Watchdog timer expire",
  "Safety timer expire",
  "Over current",
  "Calibration required"
};



// battery models (this is MY knowledge about batteries, but it can be wrong)
#define LBT_MV_MIN_LION1 2900
#define LBT_MV_TYP_LION1 3600
#define LBT_MV_MAX_LION1 4100

#define LBT_MV_MIN_LION2 3000
#define LBT_MV_TYP_LION2 3700
#define LBT_MV_MAX_LION2 4200

#define LBT_MV_MIN_LIPO1 3300
#define LBT_MV_TYP_LIPO1 3700
#define LBT_MV_MAX_LIPO1 4200

#define LBT_MV_MIN_LIPO2 3300
#define LBT_MV_TYP_LIPO2 3850
#define LBT_MV_MAX_LIPO2 4400

// battery weak state
#define LBT_MV_LOW_LION 3400
#define LBT_MV_HIGH_LION 4100

#define LBT_MV_LOW_LIPO 3600
// battery stress state
#define LBT_MV_HIGH_LIPO 4200

struct lbt_model {
  const char * tname;
  int mv_typ;
  int mv_min;
  int mv_max;
  
  int mv_low;
  int mv_high;
};

static const struct lbt_model LBTMODELS[] = {
  {LBTTYPE_[0], 0, 0, 0, 0, 0}, // Unknown
  {
    LBTTYPE_[1], // Li-ion undefined
    LBT_MV_TYP_LION1,
    LBT_MV_MIN_LION1,
    LBT_MV_MAX_LION1,
    LBT_MV_LOW_LION,
    LBT_MV_HIGH_LION
  },
  {
    LBTTYPE_[2], // Li-poly undefined
    LBT_MV_TYP_LIPO1,
    LBT_MV_MIN_LIPO1,
    LBT_MV_MAX_LIPO1,
    LBT_MV_LOW_LIPO,
    LBT_MV_HIGH_LIPO
  },
  {
    LBTTYPE_[3], // Li-ion 3.6 V
    LBT_MV_TYP_LION1,
    LBT_MV_MIN_LION1,
    LBT_MV_MAX_LION1,
    LBT_MV_LOW_LION,
    LBT_MV_HIGH_LION
  },
  {
    LBTTYPE_[4], // Li-ion 3.7 V
    LBT_MV_TYP_LION2,
    LBT_MV_MIN_LION2,
    LBT_MV_MAX_LION2,
    LBT_MV_LOW_LION,
    LBT_MV_HIGH_LION
  },
  {
    LBTTYPE_[5], // Li-poly 3.7 V
    LBT_MV_TYP_LIPO1,
    LBT_MV_MIN_LIPO1,
    LBT_MV_MAX_LIPO1,
    LBT_MV_LOW_LIPO,
    LBT_MV_HIGH_LIPO
  },
  {
    LBTTYPE_[6], // Li-poly 3.85 V
    LBT_MV_TYP_LIPO2,
    LBT_MV_MIN_LIPO2,
    LBT_MV_MAX_LIPO2,
    LBT_MV_LOW_LIPO,
    LBT_MV_HIGH_LIPO
  },
};

#endif

