/* le-battery.h - Leshto Battery (LeBattery) type
 * Author: Cledson F. Cavalcanti
 *
 * ns Leshto/Battery:
 * { LeBattery, LE_BATTERY, le_battery, (long names:) LBT, LBt, lbt }
 */

#ifndef _LE_BATTERY_H_
#define _LE_BATTERY_H_

#include <glib-object.h>
#include <lerr.h>  // includes stdio

/* Exactly the same as G_BEGIN_DECLS */
LE_C_PURE

#include "lbt-power-supply.h"
#include "lbt-types.h"

#define LE_BATTERY_PATH "/sys/class/power_supply/BAT0/"


/* GObject */
#define LE_TYPE_BATTERY (le_battery_get_type())
G_DECLARE_FINAL_TYPE(LeBattery, le_battery, LE, BATTERY, GObject)

LeBattery * le_battery_new(void);

/** /PATH/TO/POWER_SUPPLY/BATTERY/ */
gboolean le_battery_set_supply_path(LeBattery *, const char *);

/** If device's battery is supported */
gboolean le_battery_ok(LeBattery * self);

/** In the first time calling this, it'll check the "technology" file
 * and convert for a basic LeBatteryType.
 * Example:
 * "Li-ion" => LE_BATTERY_TYPE_LION;
 * "Li-poly" => LE_BATTERY_TYPE_LIPO;
 */
LeBatteryType le_battery_technology(LeBattery *);

/** For software-defined type */
void le_battery_set_technology(LeBattery *, LeBatteryType);

/* We can have inaccurate cell counts after 3 cells */
#define LBT_CELLS_ERROR 3
#define LBT_CELLS_ERROR_PERCENT 80

/** Detect number of battery cells (inaccurate with a battery pack
 * with more than 3 cells) */
int le_battery_cells(LeBattery *);

/** Battery status (see lbt-power-supply.h) */
LBTStatus le_battery_status(LeBattery *);

/** Health status (see lbt-power-supply.h) */
LBTHealth le_battery_health(LeBattery *);

/** Cycle count */
int le_battery_cycle_count(LeBattery *);

/** Temperature (C) */
float le_battery_temp(LeBattery *, enum lefile_error *);

/** Percent (%) */
int le_battery_percent(LeBattery *);

/** Voltage (V) */
float le_battery_voltage(
  LeBattery *,
  LeBatteryAttribute,
  enum lefile_error *);

/** Typical voltage (LeBattery defaults for our predefined types) */
float le_battery_voltage_typ(LeBattery *);

/** Charge (mAh)
 * If attribute is LBT_ATTRIBUTE_NOW or LBT_ATTRIBUTE_AVG, return
 * current charge (mA).
 */
float le_battery_charge(
  LeBattery *,
  LeBatteryAttribute,
  enum lefile_error *);

/** Energy (Wh)
 * If attribute is LBT_ATTRIBUTE_NOW or LBT_ATTRIBUTE_AVG, return
 * current power draw (W).
 */
float le_battery_energy(
  LeBattery *,
  LeBatteryAttribute,
  enum lefile_error *);

/** Some manufacturers don't follow standards, and had renamed
 * "energy" to "power" */
float le_battery_power(
  LeBattery *,
  LeBatteryAttribute,
  enum lefile_error *);

/** Current power draw (mA) */
float le_battery_current(
  LeBattery *,
  LeBatteryAttribute,
  enum lefile_error *);

/* Exactly the same as G_END_DECLS */
LE_CPP

#endif
