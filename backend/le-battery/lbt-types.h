/** lbt-types.h - types and data
 * Author: Cledson F. Cavalcanti
 */

#ifndef _LBT_TYPES_H_
#define _LBT_TYPES_H_

/* not used until now */
typedef enum {
  LBT_POWER_UNIT_UNKNOWN = 0,
  LBT_POWER_UNIT_AMP,
  LBT_POWER_UNIT_WATT
} LeBatteryPower;

/** Pick voltage/charge/energy/current attribute
 * We need to check attribute for LBT_ATTRIBUTE_VOLTAGE_ONLY */
typedef enum {
  LBT_ATTRIBUTE_MIN,
  LBT_ATTRIBUTE_MAX,
  LBT_ATTRIBUTE_NOW,
  LBT_ATTRIBUTE_AVG,
  LBT_ATTRIBUTE_MIN_DESIGN,
  LBT_ATTRIBUTE_MAX_DESIGN,

  // VOLTAGE ONLY:
  LBT_ATTRIBUTE_VOLTAGE_LOW,
  LBT_ATTRIBUTE_VOLTAGE_HIGH,
  LBT_ATTRIBUTE_VOLTAGE_TYP
  
} LeBatteryAttribute;
#define LBT_ATTRIBUTE_VOLTAGE_ONLY LBT_ATTRIBUTE_VOLTAGE_LOW

typedef enum le_battery_type LeBatteryType;
typedef enum lbt_status LBTStatus;
typedef enum lbt_health LBTHealth;

typedef struct lbt_type_model LBTTypeModel;

#endif

