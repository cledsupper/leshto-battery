/** leutils.h - general functions
 * Author: Cledson F. Cavalcanti
 */
#ifndef _LE_UTILS_H_
#define _LE_UTILS_H_

#include <stddef.h>

#define le_array_size(ARRAY) (sizeof(ARRAY)/sizeof(*(ARRAY)))

/** Comparison ignoring case. */
int stricmp(const char * s1, const char * s2);

#endif /* _LE_UTILS_H_ */

