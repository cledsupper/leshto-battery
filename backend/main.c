/* main.c - LeBatt command line utility
 * Author: Cledson F. Cavalcanti
 *
 * LeBatt is a command line utility used by Leshto Battery
 * application for gathering battery data and setting features.
 *
 * Please see the help or docs (if available) for getting
 * knowledge.
 */
#include <stdio.h>
#include <string.h>

#include "lebatt.h" // LeBatt driver for getting battery data
#include "leutils.h" // stricmp() for ignoring How thE User TYpeS

void lebatt_help(char * a0) {
  puts("AJUDA DO LESHTO BATTERY CLI (LEBATT-CLI)");
  printf("USO: %s [status, set-conservation, [v0, v1]]\n",
    a0);
  puts("\n * status[1]:");
  puts(" -> percent - percentual de carga. Detalhe:");
  puts(" -> voltage - tensão. Detalhe:");
  puts(" -> current - velocidade de (des)carga. Detalhe:");
  puts(" -> temp - temperatura da bateria");

  puts("\n * set-conservation <enabled, ... > - ativa/desativa o modo de conservação[2]");

  puts("\n * vB - nível de detalhe do próximo comando (B: 0 = menor; 1 = maior)");

  puts("\nANOTAÇÕES");
  puts("[1] - nem todas as opções estarão disponíveis");
  puts("[2] - o modo de conservação está disponível apenas para o Ideapad");

  puts("\nLeshtoBatt (C) 2020 (cledsonitgames EM gmail PONTO com)");
}

void test(LeBattery * batt) {
  puts(" ------ TESTING PERCENT -------\n");
  int p = le_battery_percent(batt);
  printf("%d\n", p);
  puts(" ------- TESTING ENDED --------\n");
}

int main(int argc, char * argv[]) {
  int an=1;
  bool fail = false;
  LeBattery * batt = NULL;
  lebatt_use(&batt);

  test(batt);

  while (an+1 < argc) {
    if (!stricmp(argv[an], "status")) {
      int ao = an;
      while (++ao < argc) {
        if (!stricmp(argv[ao], "percent"))
          lebatt_status_percent(batt);
        else if (!stricmp(argv[ao], "voltage"))
          lebatt_status_voltage(batt);
        else if (!stricmp(argv[ao], "current"))
          lebatt_status_current(batt);
        else if (!stricmp(argv[ao], "conservation"))
          lebatt_status_conservation(batt);
        else break;
      }
      an = ao;
    }

    else if (!stricmp(argv[an], "set-conservation")) {
      bool turn = !stricmp(argv[an+1], "enabled");
      bool status = lebatt_set_conservation(turn);
      if (status != turn) {
        fprintf(stderr, "Erro ao ativar o modo de conservação.\nVocê é root?\n");
        errno = ENOENT;
        break;
      }
      an+=2;
    }

    else { fail = true; break; }
  }

  if (fail || argc == 1) lebatt_help(argv[0]);
  lebatt_unuse(&batt);

  return errno;
}

