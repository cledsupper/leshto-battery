#ifndef _LERR_H_
#define _LERR_H_

#include <errno.h>
#include <stdio.h>

#define TRACERR() fprintf(stderr, "\t at " __FILE__ "[L%d]:%s()\n",\
  __LINE__, __func__)

#ifndef NDEBUG
#define LOG(MSG, ...) printf(__FILE__ "[L%d]:%s(): " MSG,\
  __LINE__, __func__ ,##__VA_ARGS__)
#else
#define LOG(MSG, ...)
#endif

/* For supporting compiling on C++ */
#ifndef __cplusplus
#define LE_C_PURE
#define LE_CPP
#else
#define LE_C_PURE extern "C" {
#define L_CPP }
#endif

LE_C_PURE

enum lefile_error {
  LEFILE_ERROR_NONE = 0,
  LEFILE_ERROR_ACCESS,
  LEFILE_ERROR_READ,
  LEFILE_ERROR_WRITE
};

LE_CPP

#endif
