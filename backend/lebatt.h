/* lebatt.h - LeBatt library
 * Author: Cledson F. Cavalcanti
 */
#ifndef _LEBATT_H_
#define _LEBATT_H_

#include <stdbool.h>
#include <errno.h>

#include "le-battery/le-battery.h"


/** Set battery directory (/PATH/TO/POWER_SUPPLY/BATTERY/)
 *
 * Returns:
 * If path could be changed */
bool lebatt_battery_path_set(LeBattery * bat, const char *path);

/** Create LeBattery object */
void lebatt_use(LeBattery **);

/** Destroy LeBattery object */
void lebatt_unuse(LeBattery **);

/** Return battery percent */
int lebatt_status_percent(LeBattery *);

/** Return battery voltage */
float lebatt_status_voltage(LeBattery *);

/** Return battery current (mA) */
int lebatt_status_current(LeBattery *);

/** Return if conservation mode is enabled (Ideapad only) */
bool lebatt_status_conservation();

/** Set conservation mode (Ideapad only) */
bool lebatt_set_conservation(bool enable);


#endif
