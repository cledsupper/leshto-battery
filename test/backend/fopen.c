#include <errno.h>
#include <stdio.h>
#include <string.h>

int main() {
  FILE * f = fopen("fopen.txt", "r+");
  if (f) {
    /* In a break, I can check if the file still has its
     * contents */
    getchar();
    fputs("1\n", f);
    fclose(f);
  }
  
  return errno;
}
