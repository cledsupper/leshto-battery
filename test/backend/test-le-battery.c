/** test-le-battery.c - program for testing LeBattery
 *
 * COMPILING:
 * $ cc test-le-battery.c -I../../backend/ `pkg-config gobject-2.0 --cflags` -c
 * $ cc ../../backend/le-battery/le-battery.c -I../../backend `pkg-config gobject-2.0 --cflags` -c
 * $ cc ../../backend/leutils.c -c
 */

#include <le-battery/le-battery.h>

#include <stdio.h>

int main() {
  LeBattery * b = le_battery_new();
  puts("\n ==== BATTERY INFOS ==== \n");
  printf("NÍVEL: %d %%\n", le_battery_percent(b));
  printf("TENSÃO: %.2f V\n", le_battery_voltage(b, LBT_ATTRIBUTE_NOW));
  printf("TEMPERATURA: %.1f C\n", le_battery_temp(b));
  g_object_unref(b);
  
  return 0;
}
