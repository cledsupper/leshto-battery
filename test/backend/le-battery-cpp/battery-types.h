/* battery-types - types and values mostly according to sysfs' power supply class
 * Author: Cledson F. Cavalcanti
 * Reference (last visited on July 7. 2020):
 * -> https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power
 * -> https://www.kernel.org/doc/html/latest/power/power_supply_class.html
 * -> https://batteryuniversity.com/learn/article/types_of_lithium_ion
 */

/** Default battery path */
static const char PATH[] = "/sys/class/power_supply/BAT0/";
/** Power supply type */
static const char SUPPLY[] = "Battery";

enum techno_t {
	TECHNO_UNKNOWN = 0,
	// batteries not supported by LeBattery
	TECHNO_UNSUPPORTED,
	// Li-ion-like batteries
	TECHNO_LION,
	// Li-polymer batteries
	TECHNO_LIPO,

	/* defined types (no power_supply docummented type) */

	// 3.6 V batteries (according to BatteryUniversity)
	TECHNO_LION_3_6,
	// 3.7 V batteries (according to BatteryUniversity)
	TECHNO_LION_3_7,

	// 3.7 V batteries (the same as Ideapad battery type)
	TECHNO_LIPO_3_7,
	// 3.8 V batteries (the same as Moto E battery type)
	TECHNO_LIPO_3_8,
	// 3.85 V batteries (almost all 4.000+ mAh Li-poly batteries)
	TECHNO_LIPO_3_85
};

/* TODO: CHECK THIS MAGIC NUMBER BEFORE COMPILING */
static const int TECHNO_DEFINED = 4;

static const char *const TECHNO_STR[] = {
	"Unknown",
	"Unsupported on LeBatt",
	"Li-ion",
	"Li-poly",
	"Li-ion 3.6 Volts (by LeBatt)",
	"Li-ion 3.7 Volts (by LeBatt)",
	"Li-poly 3.7 Volts (by LeBatt)",
	"Li-poly 3.8 Volts (by LeBatt)",
	"Li-poly 3.85 Volts (by LeBatt)"
};

enum status_t {
	STATUS_UNKNOWN = 0,
	STATUS_CHARGING,
	STATUS_DISCHARGING,
	STATUS_NOT_CHARGING,
	STATUS_FULL
};

static const char *const STATUS_STR[] = {
	"Unknown",
	"Charging",
	"Discharging",
	"Not charging",
	"Full"
};

enum health_t {
	HEALTH_UNKNOWN = 0,
	HEALTH_GOOD,
	HEALTH_OVERHEAT,
	HEALTH_DEAD,
	HEALTH_OVER_VOLTAGE,
	HEALTH_UNSPECIFIED_FAILURE,
	HEALTH_COLD,
	HEALTH_WATCHDOG_TIMER_EXPIRE,
	HEALTH_SAFETY_TIMER_EXPIRE,
	HEALTH_OVER_CURRENT,
	HEALTH_CALIBRATION_REQUIRED
};

static const char *const HEALTH_STR[] = {
	"Unknown",
	"Good",
	"Overheat",
	"Dead",
	"Over voltage",
	"Unspecified failure",
	"Cold",
	"Watchdog timer expire",
	"Safety timer expire",
	"Over current",
	"Calibration required"
};

enum attr_t {
	ATTR_MIN, // charge_min
	ATTR_MAX, // charge_max
	ATTR_NOW, // charge_now
	ATTR_AVG, // charge_avg
	ATTR_MIN_DESIGN, // charge_min_design
	ATTR_MAX_DESIGN // charge_max_design
};

static const char *const VOLTAGE[] = {
	"voltage_min",
	"voltage_max",
	"voltage_now",
	"voltage_avg",
	"voltage_min_design",
	"voltage_max_design"
};

static const char *const CHARGE[] = {
	"charge_empty",
	"charge_full",
	"charge_now",
	"charge_avg",
	"charge_empty_design",
	"charge_full_design"
};

static const char *const ENERGY[] = {
	"energy_empty",
	"energy_full",
	"energy_now",
	"energy_avg",
	"energy_empty_design",
	"energy_full_design"
};

enum rate_attr_t {
	RATE_ATTR_NOW,
	RATE_ATTR_AVG
};

static const char *const CURRENT[] = {
	"current_now",
	"current_avg",
};

static const char *const POWER[] = {
	"power_now",
	"power_avg"
};


/* ===== LESHTO BATTERY ONLY ===== */
enum voltage_attr_t {
	VOLTAGE_ATTR_LOW,
	VOLTAGE_ATTR_HIGH,
	VOLTAGE_ATTR_TYPICAL
};

enum file_error {
	FILE_ERROR_NONE = 0,
	FILE_ERROR_ACCESS,
	FILE_ERROR_READ,
	FILE_ERROR_WRITE
};

