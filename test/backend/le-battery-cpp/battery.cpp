#include "battery.h"

using namespace leshto;

#include <lerr.h>
#include <cerrno>
#include <cstring>
#include <string>
#include <fstream>

using namespace std;

#include <dirent.h>

static void fix_battery_path(string& path) {
	if (path[path.length()-1] != '/')
		path += "/";
	LOG("path: %s\n", path.c_str());
}

static bool can_access(const string folder) {
	DIR * dir = opendir(path);
	if (dir) {
		closedir(dir);
		return true;
	}
	return false;
}

static bool valid_type(const string path) {
	ifstream ifs(path + "type");
	char text[10] = "";
	if (!(ifs.is_open())) return false;

	ifs.getline(text, 10);
	ifs.close();

	return !strcmp(text, battery::SUPPLY)
}

battery::battery() {
	path = PATH;
	if (!can_access(path))
		cerr << __func__ << "(): "
			<< strerror(errno) << "\n";
	if (!valid_type(path) || get_technology() == TECHNO_UNSUPPORTED)
		cerr << __func__ << "(): "
			<< TECHNO_STR[TECHNO_UNSUPPORTED] << "\n";
	tech = get_technology();
}

battery::battery(const char * path) {
	fix_battery_path(path);
	this->path = path;
	if (!can_access(path))
		cerr << __func__ << "(): "
			<< strerror(errno) << "\n";
	if (!valid_type(path) || get_technology() <= TECHNO_UNSUPPORTED)
		cerr << __func__ << "(): "
			<< TECHNO_STR[TECHNO_UNSUPPORTED] << "\n";
}

void battery::read_file(const string name, long& raw) {
	ifstream ifs(path + name);
	bool io_fail;
	if (!(ifs.is_open()))
		throw FILE_ERROR_ACCESS;

	ifs >> raw;
	io_fail = ifs.fail() || ifs.bad();
	ifs.close();

	if (io_fail)
		throw FILE_ERROR_READ;
}

void battery::read_file(const string name, char ** raw) {
	ifstream ifs(path + name);
	if (!(ifs.is_open()))
		throw FILE_ERROR_ACCESS;

	ifs.seekg(0, ifs.end);
	int length = ifs.tellg();
	ifs.seekg(0, ifs.beg);

	*raw = new char[length];
	ifs.getline(*raw, length);

	bool io_fail = ifs.fail() || ifs.bad();
	ifs.close();

	if (io_fail)
		throw FILE_ERROR_READ;
}

battery::techno_t battery::get_technology() {
	return TECHNO_UNKNOWN;
}

battery::status_t battery::get_status() {
	return STATUS_UNKNOWN;
}

battery::health_t battery::get_health() {
	return HEALTH_UNKNOWN;
}

int battery::get_percent() {
	return 0;
}

double battery::get_voltage(battery::attr_t a) {
	return 0;
}

double battery::get_voltage_lebatt(battery::voltage_attr_t a) {
	return 0;
}

int battery::get_voltage_perc() {
	return 0;
}

double battery::get_charge(battery::attr_t a) {
	return 0;
}

double battery::get_energy(battery::attr_t a) {
	return 0;
}

double battery::get_current(battery::rate_attr_t a) {
	return 0;
}

double battery::get_power(battery::rate_attr_t a) {
	return 0;
}

