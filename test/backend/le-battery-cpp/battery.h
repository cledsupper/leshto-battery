/** battery.h - leshto::battery class
 * Author: Cledson F. Cavalcanti
 */
#ifndef _LESHTO_BATTERY_H_
#define _LESHTO_BATTERY_H_

#include <string>

namespace leshto {

	class battery {
	public:
		#include "battery-types.h"

		battery();
		battery(const char * alt_path);

		techno_t get_technology() throw (file_error);
		status_t get_status() throw (file_error);
		health_t get_health() throw (file_error);

		int get_percent() throw (file_error);
		double get_voltage(attr_t) throw (file_error);
		double get_voltage_lebatt(voltage_attr_t);
		double get_voltage_perc() throw (file_error);

		double get_charge(attr_t) throw (file_error);
		double get_energy(attr_t) throw (file_error);

		double get_current(rate_attr_t) throw (file_error);
		double get_power(rate_attr_t) throw (file_error);

		void set_technology(techno_t);

	private:
		void read_file(const string, long& raw)
			throw (file_error);
		void read_file(const string, char ** line)
			throw (file_error);

		techno_t tech;
		std::string path;
		bool exists;
	};

}

#endif
